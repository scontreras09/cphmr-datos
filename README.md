# CPHMR Datos

La educación es uno de los pilares de toda sociedad, y por lo tanto tener medidas para evaluar sus resultados y proponer medidas para mejorar es de vital importancia. En Chile, el Ministerio de Educación, a través de su Centro de Estudios, mantiene una plataforma de datos abiertos con una importante cantidad de información relativa a estudiantes, docentes y establecimientos, en un esfuerzo por transparentar los resultados de la importante labor a su cargo.

Este repositorio contiene hitos alcanzados en el ánalisis exploratorio y minería de datos sobre un dataset con resultados académicos obtenidos por estudiantes de enseñanza básica y media durante el año escolar 2021, con la intención de encontrar regularidades y patrones que sean de interés para los propios estudiantes y el país en general.

Dada la granularidad de los datos, es posible segregar la información por comunas o regiones, género de los estudiantes, tipos de establecimientos y ruralidad, entre otras variables, que esperamos puedan combinarse de manera interesante y útil para la toma de decisiones futuras.
